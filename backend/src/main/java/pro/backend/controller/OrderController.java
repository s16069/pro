package pro.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;
import pro.backend.dto.OrderDto;
import pro.backend.model.order.Order;
import pro.backend.model.order.OrderStatus;
import pro.backend.model.user.Roles;
import pro.backend.service.OrderService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/orders")
@Secured({Roles.ROLE_CLIENT, Roles.ROLE_RESTAURANT_EMPLOYEE, Roles.ROLE_RESTAURANT_ADMIN})
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping
    public ResponseEntity<Order> addOrder(@RequestBody @Valid OrderDto orderDto, Authentication authentication) {

        String userId = authentication.getName();

        Optional<Order> order = orderService.addOrder(userId, orderDto.getPizzas(), orderDto.getAddress(), orderDto.getComments());

        return ResponseEntity.of(order);
    }

    @GetMapping
    public List<Order> listOrders(Authentication authentication) {

        String userId = authentication.getName();
        List<String> roles = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return orderService.list(userId, roles);
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<Order> getOrder(Authentication authentication, @PathVariable long orderId) {

        String userId = authentication.getName();
        List<String> roles = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return ResponseEntity.of(orderService.get(userId, roles, orderId));
    }

    @PutMapping("/{orderId}/status")
    @Secured({Roles.ROLE_RESTAURANT_EMPLOYEE, Roles.ROLE_RESTAURANT_ADMIN})
    public ResponseEntity<Order> changeOrderStatus(@PathVariable long orderId, @RequestParam OrderStatus status) {

        return ResponseEntity.of(orderService.changeStatus(orderId, status));
    }
}