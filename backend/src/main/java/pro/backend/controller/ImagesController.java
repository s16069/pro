package pro.backend.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pro.backend.dto.ImageDto;
import pro.backend.model.Image;
import pro.backend.model.user.Roles;
import pro.backend.service.ImageService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/images")
public class ImagesController {

    @Autowired
    private ImageService imageService;

    @Autowired
    private ModelMapper modelMapper;


    @GetMapping("/{id}")
    public ResponseEntity<byte[]> getImage(@PathVariable("id") long id) {
        Optional<Image> image = imageService.findById(id);

        return image.map(this::createResponse).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping
    @Secured(Roles.ROLE_RESTAURANT_ADMIN)
    public List<ImageDto> listImages() {
        return imageService.listAll().stream().map(this::convert).collect(Collectors.toList());
    }


    private ImageDto convert(Image image) {
        return modelMapper.map(image, ImageDto.class);
    }

    private ResponseEntity<byte[]> createResponse(Image image) {
        return createResponse(image.getData(), String.valueOf(image.getId()), image.getMime());
    }

    private ResponseEntity<byte[]> createResponse(byte[] data, String name, String mime) {
        String fileName = name + "." + mime.split("/")[1];

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE, mime);
        headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        headers.setContentLength(data.length);

        return new ResponseEntity<>(data, headers, HttpStatus.OK);
    }

}
