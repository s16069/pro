package pro.backend.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pro.backend.model.menu.Promotion;
import pro.backend.model.user.Roles;
import pro.backend.service.PromotionService;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/admin/promotions")
@Secured(Roles.ROLE_RESTAURANT_ADMIN)
public class AdminPromotionController {

    @Autowired
    private PromotionService promotionService;

    @PostMapping
    public ResponseEntity<Promotion> addPromotion(@RequestBody @Valid Promotion promotion) {
        Promotion added = promotionService.add(promotion);
        return ResponseEntity.ok().body(added);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Promotion> updatePromotion(@PathVariable("id") long id, @RequestBody @Valid Promotion update) {
        Optional<Promotion> updated = promotionService.update(id, update);
        return ResponseEntity.of(updated);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePromotion(@PathVariable("id") long id) {
        promotionService.delete(id);
        return ResponseEntity.ok().body(null);
    }
}
