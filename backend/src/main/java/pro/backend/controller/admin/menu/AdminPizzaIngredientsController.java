package pro.backend.controller.admin.menu;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pro.backend.dto.menu.pizza.MenuPizzaDto;
import pro.backend.model.menu.pizza.MenuPizza;
import pro.backend.model.user.Roles;
import pro.backend.service.PizzaService;

@RestController
@RequestMapping("/admin/menu/pizzas/{pizzaId}/ingredients")
@Secured(Roles.ROLE_RESTAURANT_ADMIN)
public class AdminPizzaIngredientsController {

    @Autowired
    private PizzaService pizzaService;

    @Autowired
    private ModelMapper modelMapper;

    @PutMapping("/{ingredientId}")
    public ResponseEntity<MenuPizzaDto> addPizzaIngredient(@PathVariable("pizzaId") long pizzaId, @PathVariable("ingredientId") long ingredientId) {
        return ResponseEntity.of(pizzaService.addIngredient(pizzaId, ingredientId).map(this::convert));
    }

    @DeleteMapping("/{ingredientId}")
    public ResponseEntity<MenuPizzaDto> deletePizzaIngredient(@PathVariable("pizzaId") long pizzaId, @PathVariable("ingredientId") long ingredientId) {
        return ResponseEntity.of(pizzaService.deleteIngredient(pizzaId, ingredientId).map(this::convert));
    }

    private MenuPizzaDto convert(MenuPizza menuPizza) {
        return modelMapper.map(menuPizza, MenuPizzaDto.class);
    }
}