package pro.backend.controller.admin.menu;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pro.backend.dto.menu.pizza.PizzaDoughDto;
import pro.backend.model.menu.pizza.PizzaDough;
import pro.backend.model.user.Roles;
import pro.backend.service.DoughService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin/menu/doughs")
@Secured(Roles.ROLE_RESTAURANT_ADMIN)
public class AdminDoughController {

    @Autowired
    private DoughService doughService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public List<PizzaDoughDto> listDoughs() {
        return doughService.list().stream().map(this::convert).collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<PizzaDoughDto> add(@RequestBody @Valid PizzaDoughDto doughDto) {
        PizzaDough dough = doughService.add(convert(doughDto));
        return ResponseEntity.ok().body(convert(dough));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PizzaDoughDto> update(@PathVariable("id") long id, @RequestBody @Valid PizzaDoughDto updateDto) {
        Optional<PizzaDough> dough = doughService.update(id, convert(updateDto));
        return ResponseEntity.of(dough.map(this::convert));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") long id) {
        doughService.delete(id);
        return ResponseEntity.ok().body(null);
    }

    private PizzaDoughDto convert(PizzaDough dough) {
        return modelMapper.map(dough, PizzaDoughDto.class);
    }

    private PizzaDough convert(PizzaDoughDto doughDto) {
        return modelMapper.map(doughDto, PizzaDough.class);
    }

}
