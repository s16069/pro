package pro.backend.controller.admin.menu;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pro.backend.dto.menu.pizza.MenuPizzaDto;
import pro.backend.dto.menu.pizza.PizzaDoughDto;
import pro.backend.model.menu.pizza.MenuPizza;
import pro.backend.model.menu.pizza.PizzaDough;
import pro.backend.model.user.Roles;
import pro.backend.service.PizzaService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/admin/menu/pizzas")
@Secured(Roles.ROLE_RESTAURANT_ADMIN)
public class AdminPizzaController {

    @Autowired
    private PizzaService pizzaService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public List<MenuPizza> listPizzas() {
        return pizzaService.list();
    }

    @GetMapping("/{id}")
    public ResponseEntity<MenuPizzaDto> getPizza(@PathVariable("id") long id) {
        return ResponseEntity.of(pizzaService.findById(id).map(this::convert));
    }

    @PostMapping
    public ResponseEntity<MenuPizzaDto> addPizza(@RequestBody @Valid MenuPizzaDto menuPizza) {
        MenuPizza pizza = pizzaService.add(convert(menuPizza));
        return ResponseEntity.ok().body(convert(pizza));
    }

    @PutMapping("/{id}")
    public ResponseEntity<MenuPizzaDto> updatePizza(@PathVariable("id") long id, @RequestBody @Valid MenuPizzaDto update) {
        Optional<MenuPizza> pizza = pizzaService.update(id, convert(update));
        return ResponseEntity.of(pizza.map(this::convert));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePizza(@PathVariable("id") long id) {
        pizzaService.delete(id);
        return ResponseEntity.ok().body(null);
    }

    private MenuPizzaDto convert(MenuPizza menuPizza) {
        return modelMapper.map(menuPizza, MenuPizzaDto.class);
    }

    private MenuPizza convert(MenuPizzaDto menuPizzaDto) {
        return modelMapper.map(menuPizzaDto, MenuPizza.class);
    }
}