package pro.backend.controller.admin.menu;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pro.backend.dto.menu.pizza.PizzaDoughDto;
import pro.backend.dto.menu.pizza.PizzaSizeDto;
import pro.backend.model.menu.pizza.PizzaDough;
import pro.backend.model.menu.pizza.PizzaSize;
import pro.backend.model.user.Roles;
import pro.backend.service.SizeService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin/menu/sizes")
@Secured(Roles.ROLE_RESTAURANT_ADMIN)
public class AdminSizeController {

    @Autowired
    private SizeService sizeService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public List<PizzaSizeDto> listSizes() {
        return sizeService.list().stream().map(this::convert).collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<PizzaSizeDto> add(@RequestBody @Valid PizzaSizeDto sizeDto) {
        PizzaSize size = sizeService.add(convert(sizeDto));
        return ResponseEntity.ok().body(convert(size));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PizzaSizeDto> update(@PathVariable("id") long id, @RequestBody @Valid PizzaSizeDto sizeDto) {
        Optional<PizzaSize> size = sizeService.update(id, convert(sizeDto));
        return ResponseEntity.of(size.map(this::convert));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") long id) {
        sizeService.delete(id);
        return ResponseEntity.ok().body(null);
    }

    private PizzaSizeDto convert(PizzaSize size) {
        return modelMapper.map(size, PizzaSizeDto.class);
    }

    private PizzaSize convert(PizzaSizeDto sizeDto) {
        return modelMapper.map(sizeDto, PizzaSize.class);
    }
}