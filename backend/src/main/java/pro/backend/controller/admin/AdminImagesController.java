package pro.backend.controller.admin;

import org.apache.commons.codec.binary.Base64;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import pro.backend.dto.ImageDto;
import pro.backend.dto.UploadImageDto;
import pro.backend.model.Image;
import pro.backend.model.user.Roles;
import pro.backend.service.ImageService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin/images")
public class AdminImagesController {

    @Autowired
    private ImageService imageService;

    @Autowired
    private ModelMapper modelMapper;


    @PostMapping
    public ImageDto uploadImage(@RequestBody UploadImageDto imageDto, Authentication authentication) throws IOException {
        String userId = authentication.getName();

        byte[] imageData = Base64.decodeBase64(imageDto.getFileData());
        InputStream inputStream = new ByteArrayInputStream(imageData);

        Image image = imageService.add(imageDto.getFileName(), imageDto.getAlt(), userId, inputStream);

        return convert(image);
    }

    @GetMapping
    @Secured(Roles.ROLE_RESTAURANT_ADMIN)
    public List<ImageDto> listImages() {
        return imageService.listAll().stream().map(this::convert).collect(Collectors.toList());
    }


    private ImageDto convert(Image image) {
        return modelMapper.map(image, ImageDto.class);
    }

}
