package pro.backend.controller.admin.menu;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pro.backend.dto.menu.pizza.MenuPizzaDto;
import pro.backend.dto.menu.pizza.PizzaPriceDto;
import pro.backend.model.menu.pizza.MenuPizza;
import pro.backend.model.menu.pizza.PizzaPrice;
import pro.backend.model.user.Roles;
import pro.backend.service.PizzaService;

import javax.validation.Valid;

@RestController
@RequestMapping("/admin/menu/pizzas/{pizzaId}/prices")
@Secured(Roles.ROLE_RESTAURANT_ADMIN)
public class AdminPizzaPricesController {

    @Autowired
    private PizzaService pizzaService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<PizzaPriceDto> addPizzaPrice(@PathVariable("pizzaId") long pizzaId, @RequestBody @Valid PizzaPrice pizzaPrice) {
        return ResponseEntity.of(pizzaService.addPrice(pizzaId, pizzaPrice).map(this::convert));
    }

    @PutMapping("/{priceId}")
    public ResponseEntity<PizzaPriceDto> updatePizzaPrice(@PathVariable("pizzaId") long pizzaId, @PathVariable("priceId") long priceId, @RequestBody @Valid PizzaPrice update) {
        return ResponseEntity.of(pizzaService.updatePrice(pizzaId, priceId, update).map(this::convert));
    }

    @DeleteMapping("/{priceId}")
    public ResponseEntity<Void> deletePizzaPrice(@PathVariable("pizzaId") long pizzaId, @PathVariable("priceId") long priceId) {
        pizzaService.deletePrice(pizzaId, priceId);
        return ResponseEntity.ok().body(null);
    }

    private PizzaPriceDto convert(PizzaPrice pizzaPrice) {
        return modelMapper.map(pizzaPrice, PizzaPriceDto.class);
    }

}