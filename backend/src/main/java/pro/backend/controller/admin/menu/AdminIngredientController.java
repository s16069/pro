package pro.backend.controller.admin.menu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pro.backend.model.menu.pizza.Ingredient;
import pro.backend.model.user.Roles;
import pro.backend.service.IngredientService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/admin/ingredients")
@Secured(Roles.ROLE_RESTAURANT_ADMIN)
public class AdminIngredientController {

    @Autowired
    private IngredientService ingredientService;

    @GetMapping
    public List<Ingredient> listIngredients() {
        return ingredientService.list();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Ingredient> getIngredient(@PathVariable("id") long id) {
        return ResponseEntity.of(ingredientService.findById(id));
    }

    @PostMapping
    public ResponseEntity<Ingredient> addIngredient(@RequestBody @Valid Ingredient ingredient) {
        Ingredient added = ingredientService.add(ingredient);
        return ResponseEntity.ok().body(added);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Ingredient> updateIngredient(@PathVariable("id") long id, @RequestBody @Valid Ingredient update) {
        Optional<Ingredient> updated = ingredientService.update(id, update);
        return ResponseEntity.of(updated);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") long id) {
        ingredientService.delete(id);
        return ResponseEntity.ok().body(null);
    }
}
