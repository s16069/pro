package pro.backend.controller.menu;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pro.backend.dto.menu.pizza.PizzaSizeDto;
import pro.backend.model.menu.pizza.PizzaSize;
import pro.backend.service.SizeService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/menu/sizes")
public class SizeController {

    @Autowired
    private SizeService sizeService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public List<PizzaSizeDto> listSizes() {

        List<PizzaSizeDto> sizes = sizeService.list().stream().map(this::convert).collect(Collectors.toList());

        return sizes;
    }

    private PizzaSizeDto convert(PizzaSize size) {
        return modelMapper.map(size, PizzaSizeDto.class);
    }
}