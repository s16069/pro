package pro.backend.controller.menu;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pro.backend.dto.menu.product.MenuProductDto;
import pro.backend.model.menu.product.MenuProduct;
import pro.backend.service.ProductService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/menu/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public List<MenuProductDto> listMenu() {
        return productService.listProducts().stream().map(this::convert).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<MenuProductDto> get(@PathVariable("id") long id) {
        return ResponseEntity.of(productService.findById(id).map(this::convert));
    }

    private MenuProductDto convert(MenuProduct product) {
        return modelMapper.map(product, MenuProductDto.class);
    }
}