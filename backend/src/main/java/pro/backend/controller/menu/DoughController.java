package pro.backend.controller.menu;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pro.backend.dto.menu.pizza.PizzaDoughDto;
import pro.backend.model.menu.pizza.PizzaDough;
import pro.backend.service.DoughService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/menu/doughs")
public class DoughController {

    @Autowired
    private DoughService doughService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public List<PizzaDoughDto> listDoughs() {

        List<PizzaDoughDto> doughs = doughService.list().stream().map(this::convert).collect(Collectors.toList());

        return doughs;
    }

    private PizzaDoughDto convert(PizzaDough dough) {
        return modelMapper.map(dough, PizzaDoughDto.class);
    }
}