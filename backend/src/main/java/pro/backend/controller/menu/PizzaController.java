package pro.backend.controller.menu;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pro.backend.dto.menu.PriceRangeDto;
import pro.backend.dto.menu.pizza.MenuPizzaDto;
import pro.backend.model.menu.pizza.MenuPizza;
import pro.backend.service.PizzaService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/menu/pizzas")
public class PizzaController {

    @Autowired
    private PizzaService pizzaService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public List<MenuPizzaDto> listPizzas() {
        return pizzaService.list().stream().map(this::convert).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<MenuPizzaDto> getPizza(@PathVariable("id") long id) {
        return ResponseEntity.of(pizzaService.findById(id).map(this::convert));
    }

    private MenuPizzaDto convert(MenuPizza pizza) {
        MenuPizzaDto pizzaDto = modelMapper.map(pizza, MenuPizzaDto.class);
        pizzaDto.setPriceRange(modelMapper.map(pizza.getPriceRange(), PriceRangeDto.class));
        return pizzaDto;
    }
}