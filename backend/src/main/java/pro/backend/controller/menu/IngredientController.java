package pro.backend.controller.menu;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pro.backend.dto.menu.pizza.IngredientDto;
import pro.backend.model.menu.pizza.Ingredient;
import pro.backend.service.IngredientService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/menu/ingredients")
public class IngredientController {

    @Autowired
    private IngredientService ingredientService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public List<IngredientDto> listIngredients() {
        return ingredientService.list().stream().map(this::convert).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<IngredientDto> getIngredient(@PathVariable("id") long id) {
        return ResponseEntity.of(ingredientService.findById(id).map(this::convert));
    }

    private IngredientDto convert(Ingredient ingredient) {
        return modelMapper.map(ingredient, IngredientDto.class);
    }
}
