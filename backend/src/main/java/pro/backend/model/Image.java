package pro.backend.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "Image")
@Getter
@Setter
@NoArgsConstructor
public class Image {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = true)
    private String alt;

    @Column(nullable = false)
    private String mime;

    @Column(nullable = true)
    private int width;

    @Column(nullable = true)
    private int height;

    @Lob
    //@Column(columnDefinition="BLOB")  // H2
    @Basic(fetch = FetchType.LAZY)
    private byte[] data;

    @Column(nullable = false)
    private int size;

    @Column(nullable = false)
    private Timestamp timeCreated;

//    @Column(columnDefinition = "uuid", nullable = true)
//    private String userId;
}
