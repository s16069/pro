package pro.backend.model.menu;

import lombok.*;
import pro.backend.model.menu.product.MenuProduct;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "MenuCategory")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class MenuCategory {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @NonNull
    @Column(nullable = false)
    private String name;

    @NonNull
    @Column(nullable = false)
    private String description;

    @ManyToMany(mappedBy = "categories")
    private List<MenuProduct> products = new ArrayList<>();

}
