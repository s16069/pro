package pro.backend.model.menu.pizza;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "PizzaDough")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class PizzaDough {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @NonNull
    @Column(nullable = false)
    private String name;

    @NonNull
    @Column(nullable = false)
    private String description;

    @NonNull
    @Column(nullable = false)
    private BigDecimal priceMultiplier;
}
