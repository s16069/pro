package pro.backend.model.menu.product;


import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;


@Entity
@Table(name = "ProductPrice")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ProductPrice {

    @Id
    @GeneratedValue
    @Column(name = "id")
    protected Long id;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "product_id", nullable = false, updatable = false)
    private MenuProduct menuProduct;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "variant_id", nullable = false, updatable = false)
    private ProductVariant variant;

    @NonNull
    @Column(nullable = false)
    private BigDecimal price;
}