package pro.backend.model.menu.product;

import lombok.*;
import pro.backend.model.menu.MenuPosition;
import pro.backend.model.menu.PriceRange;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "MenuProduct")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class MenuProduct extends MenuPosition {

    @Column(nullable = false)
    private BigDecimal basePrice;

    @NonNull
    @Column
    private boolean vegetarian;

    @NonNull
    @Column
    private boolean vegan;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "variant_series_id", nullable = false, updatable = false)
    private ProductVariantSeries variantSeries;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY, mappedBy = "menuProduct")
    @OrderBy("id")
    private List<ProductPrice> productPrices = new ArrayList<>();


    public MenuProduct(@NonNull String name, @NonNull String description, BigDecimal basePrice, @NonNull boolean vegetarian, @NonNull boolean vegan, @NonNull ProductVariantSeries variantSeries) {
        super(name, description);
        this.basePrice = basePrice;
        this.vegetarian = vegetarian;
        this.vegan = vegan;
        this.variantSeries = variantSeries;
    }

    public void generatePrices() {
        productPrices = new ArrayList<>();
        for (ProductVariant variant : variantSeries.getVariants()) {
            productPrices.add(new ProductPrice(this, variant, basePrice.multiply(variant.getPriceMultiplier())));
        }
    }

    @Override
    public PriceRange getPriceRange() {
        if (productPrices.size() == 0) {
            return null;
        }

        BigDecimal min = productPrices.stream().map(ProductPrice::getPrice).min(BigDecimal::compareTo).get();
        BigDecimal max = productPrices.stream().map(ProductPrice::getPrice).max(BigDecimal::compareTo).get();

        return new PriceRange(min, max);
    }
}
