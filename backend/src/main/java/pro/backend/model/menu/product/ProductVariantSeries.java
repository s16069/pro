package pro.backend.model.menu.product;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ProductVariantSeries")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ProductVariantSeries {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @NonNull
    @Column(nullable = false)
    private String name;

    @NonNull
    @Column(nullable = false)
    private String description;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY, mappedBy = "variantSeries")
    @OrderBy("id")
    private List<ProductVariant> variants = new ArrayList<>();
}
