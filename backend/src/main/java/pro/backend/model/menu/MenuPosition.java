package pro.backend.model.menu;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.*;
import pro.backend.model.Image;
import pro.backend.model.menu.pizza.MenuPizza;
import pro.backend.model.menu.pizza.PizzaDough;
import pro.backend.model.menu.product.MenuProduct;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = MenuPizza.class, name = "pizza"),
        @JsonSubTypes.Type(value = MenuProduct.class, name = "product")
})
public abstract class MenuPosition {

    @JsonInclude()
    @Transient
    private String type;

    @Id
    @GeneratedValue
    @Column(name = "id")
    protected Long id;

    @NonNull
    @Column(nullable = false)
    protected String name;

    @NonNull
    @Column(nullable = false)
    protected String description;

    @ManyToOne(optional = true)
    @JoinColumn(name = "image_id", nullable = true, updatable = true)
    private Image image;

    @ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "MenuPosition_Promotion", joinColumns = {@JoinColumn(name = "position_id")}, inverseJoinColumns = {@JoinColumn(name = "promotion_id")})
    @OrderBy("id")
    List<Promotion> promotions = new ArrayList<>();

    @ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "MenuPosition_Category", joinColumns = {@JoinColumn(name = "position_id")}, inverseJoinColumns = {@JoinColumn(name = "category_id")})
    @OrderBy("id")
    List<MenuCategory> categories = new ArrayList<>();

    public abstract PriceRange getPriceRange();

}
