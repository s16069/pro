package pro.backend.model.menu;

import lombok.*;
import pro.backend.model.menu.product.MenuProduct;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Promotion")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class Promotion {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @NonNull
    @Column(nullable = false)
    private String name;

    @NonNull
    @Column(nullable = false)
    private PromotionType type;

    @NonNull
    @Column(nullable = false)
    private Timestamp dateFrom;

    @Column(nullable = false)
    private Timestamp dateTo;

    @NonNull
    @Column(nullable = false)
    private BigDecimal discount;

    @ManyToMany(mappedBy = "promotions")
    private List<MenuProduct> products = new ArrayList<>();

}
