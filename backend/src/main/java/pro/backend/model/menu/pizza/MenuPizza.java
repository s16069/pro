package pro.backend.model.menu.pizza;

import lombok.*;
import pro.backend.model.menu.MenuPosition;
import pro.backend.model.menu.PriceRange;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "MenuPizza")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class MenuPizza extends MenuPosition implements IngredientSet {

    @NonNull
    @Column(nullable = false)
    protected Boolean isVegetarian = false;

    @NonNull
    @Column(nullable = false)
    protected Boolean isVegan = false;

    @NonNull
    @Column(nullable = false)
    protected Boolean isSpicy = false;

    @ManyToMany(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "MenuPizza_Ingredient",
            joinColumns = {@JoinColumn(name = "pizza_id")},
            inverseJoinColumns = {@JoinColumn(name = "ingredient_id")})
    List<Ingredient> ingredients = new ArrayList<>();

    @NonNull
    @Column(nullable = false)
    private BigDecimal basePrice;

    @OneToMany(cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY, mappedBy = "menuPizza")
    @OrderBy("id")
    private List<PizzaPrice> pizzaPrices = new ArrayList<>();

    public MenuPizza(@NonNull String name, @NonNull String description, @NonNull Boolean isVegetarian, @NonNull Boolean isVegan, @NonNull Boolean isSpicy, @NonNull BigDecimal basePrice) {
        super(name, description);
        this.isVegetarian = isVegetarian;
        this.isVegan = isVegan;
        this.isSpicy = isSpicy;
        this.basePrice = basePrice;
    }

    public void generatePrices(List<PizzaSize> sizes, List<PizzaDough> doughs) {
        this.pizzaPrices = new ArrayList<>();
        for (PizzaSize pizzaSize : sizes) {
            for (PizzaDough pizzaDough : doughs) {
                BigDecimal price = basePrice.multiply(pizzaSize.getPriceMultiplier()).multiply(pizzaDough.getPriceMultiplier());
                PizzaPrice pizzaPrice = new PizzaPrice(this, pizzaDough, pizzaSize, price);
                this.pizzaPrices.add(pizzaPrice);
            }
        }
    }

    @Override
    public PriceRange getPriceRange() {
        BigDecimal min = pizzaPrices.stream().map(PizzaPrice::getPrice).min(BigDecimal::compareTo).orElse(basePrice);
        BigDecimal max = pizzaPrices.stream().map(PizzaPrice::getPrice).max(BigDecimal::compareTo).orElse(basePrice);

        return new PriceRange(min, max);
    }
}
