package pro.backend.model.menu;

public enum PromotionType {
    FIXED,
    PROPORTIONAL
}
