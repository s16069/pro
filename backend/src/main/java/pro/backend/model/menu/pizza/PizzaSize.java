package pro.backend.model.menu.pizza;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "PizzaSize")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class PizzaSize {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @NonNull
    @Column(nullable = false)
    private String name;

    @NonNull
    @Column(nullable = false)
    private int diameter;

    @NonNull
    @Column(nullable = false)
    private BigDecimal priceMultiplier;
}
