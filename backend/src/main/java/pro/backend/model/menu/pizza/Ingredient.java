package pro.backend.model.menu.pizza;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Ingredient")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class Ingredient {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @NonNull
    @Column(nullable = false)
    private String name;

    @NonNull
    @Column(nullable = false)
    private boolean spicy;

    @NonNull
    @Column(nullable = false)
    private BigDecimal price;

    @ManyToMany(mappedBy = "ingredients")
    private List<MenuPizza> pizzas = new ArrayList<>();
}
