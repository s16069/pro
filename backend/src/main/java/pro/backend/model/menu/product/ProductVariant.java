package pro.backend.model.menu.product;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ProductVariant")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ProductVariant {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @NonNull
    @Column(nullable = false)
    private String name;

    @NonNull
    @Column(nullable = false)
    private String description;

    @NonNull
    @Column(nullable = false)
    private BigDecimal priceMultiplier;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "variant_series_id", nullable = false, updatable = false)
    private ProductVariantSeries variantSeries;
}
