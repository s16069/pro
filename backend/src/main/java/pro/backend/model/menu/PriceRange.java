package pro.backend.model.menu;

import lombok.NonNull;
import lombok.Value;

import java.math.BigDecimal;

@Value
public class PriceRange {

    @NonNull
    BigDecimal from;

    @NonNull
    BigDecimal to;
}
