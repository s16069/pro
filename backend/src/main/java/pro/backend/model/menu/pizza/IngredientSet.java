package pro.backend.model.menu.pizza;

import java.util.List;

public interface IngredientSet {

    List<Ingredient> getIngredients();

    default boolean isSpicy() {
        return getIngredients().stream().anyMatch(ingredient -> ingredient.isSpicy());
    }
//
//	@JsonView(Views.Overview.class)
//	default List<String> getAllergens() {
//		return getIngredients().stream().flatMap(ingredient -> ingredient.getAllergens().stream())
//				.collect(Collectors.toList());
//	}
}
