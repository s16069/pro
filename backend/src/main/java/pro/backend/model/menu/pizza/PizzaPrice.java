package pro.backend.model.menu.pizza;


import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;


@Entity
@Table(name = "PizzaPrice")
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class PizzaPrice {

    @Id
    @GeneratedValue
    @Column(name = "id")
    protected Long id;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "pizza_id", nullable = false, updatable = false)
    private MenuPizza menuPizza;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "dough_id", nullable = false, updatable = false)
    private PizzaDough dough;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "size_id", nullable = false, updatable = false)
    private PizzaSize size;

    @NonNull
    @Column(nullable = false)
    private BigDecimal price;
}