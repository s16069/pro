package pro.backend.model.order;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import pro.backend.model.menu.pizza.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "OrderedPizza")
@Getter
@Setter
@NoArgsConstructor
public class OrderedPizza extends OrderPosition {

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(name = "OrderedPizza_Ingredient", joinColumns = {@JoinColumn(name = "ordered_pizza_id")}, inverseJoinColumns = {@JoinColumn(name = "ingredient_id")})
    List<Ingredient> ingredients;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "pizza_id", nullable = false, updatable = false)
    private MenuPizza menuPizza;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "dough_id", nullable = false, updatable = false)
    private PizzaDough dough;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "size_id", nullable = false, updatable = false)
    private PizzaSize size;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "price_id", nullable = false, updatable = false)
    private PizzaPrice price;

    @Override
    public BigDecimal getPrice() {
        return price.getPrice().multiply(new BigDecimal(amount));
    }
}
