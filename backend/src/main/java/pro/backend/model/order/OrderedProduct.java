package pro.backend.model.order;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import pro.backend.model.menu.product.MenuProduct;
import pro.backend.model.menu.product.ProductPrice;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "OrderedProduct")
@Getter
@Setter
@NoArgsConstructor
public class OrderedProduct extends OrderPosition {

    @ManyToOne(optional = false)
    @JoinColumn(name = "product_id", nullable = false, updatable = false)
    private MenuProduct menuProduct;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "price_id", nullable = false, updatable = false)
    private ProductPrice price;

    @Override
    public BigDecimal getPrice() {
        return price.getPrice().multiply(new BigDecimal(amount));
    }
}
