package pro.backend.model.order;

public enum OrderStatus {
    CREATED, ACCEPTED, PREPARED, DELIVERED
}
