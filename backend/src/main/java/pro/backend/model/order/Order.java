package pro.backend.model.order;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import pro.backend.model.user.Address;

import javax.persistence.*;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Order")
@Getter
@Setter
@NoArgsConstructor
public class Order {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    @Column(nullable = true)
    private String comments;

    @Embedded
    @Valid
    private Address address;

    @Column(nullable = false)
    private OrderStatus status;

    @Column(nullable = false)
    private Timestamp timeCreated;

    @Column(nullable = true)
    private Timestamp timeAccepted;

    @Column(nullable = true)
    private Timestamp timePrepared;

    @Column(nullable = true)
    private Timestamp timeDelivered;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "order")
    @OrderBy("id")
    @Fetch(FetchMode.JOIN)
    private List<OrderedPizza> positions = new ArrayList<>();

    @Column(columnDefinition = "uuid", nullable = false)
    private String clientId;

    public Order(OrderStatus status, String comments) {
        this();

        setStatus(status);
        setComments(comments);
    }

    public BigDecimal getPrice() {
        BigDecimal sum = new BigDecimal(0);

        for (OrderedPizza position : positions)
            sum = sum.add(position.getPrice());

        return sum;
    }

    public void addPosition(OrderedPizza position) {
        if (position == null)
            throw new NullPointerException("position cannot be null");

        if (position.getOrder() != this)
            throw new IllegalArgumentException("position belongs to other order");

        if (!positions.contains(position)) {
            this.positions.add(position);
            position.setOrder(this);
        }
    }
}
