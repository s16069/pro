package pro.backend.model.order;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@NoArgsConstructor
public abstract class OrderPosition {

    private static final int MAX_AMOUNT = 5;

    @Id
    @GeneratedValue
    @Column(name = "id")
    protected Long id;

    @Column(nullable = false)
    @Min(value = 1, message = "Zamów co najmniej jedną pozycję")
    @Max(value = MAX_AMOUNT, message = "Nie można zamówić więcej niż " + MAX_AMOUNT + " pozycji")
    protected int amount;

    @NonNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "order_id", nullable = false, updatable = false)
    protected Order order;

    public abstract BigDecimal getPrice();
}
