package pro.backend.model.user;

public class Roles {
    public static final String ROLE_CLIENT = "ROLE_CLIENT";
    public static final String ROLE_RESTAURANT_EMPLOYEE = "ROLE_RESTAURANT_EMPLOYEE";
    public static final String ROLE_RESTAURANT_ADMIN = "ROLE_RESTAURANT_ADMIN";
}
