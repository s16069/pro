package pro.backend.model.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
public class Address {

    @Column
    @NotNull
    @Size(min = 2, message = "Minimalnie 2 znaki")
    @Size(max = 16, message = "Maksymalnie 16 znaków")
    private String city;

    @Column
    @NotNull
    @Size(min = 2, message = "Minimalnie 2 znaki")
    @Size(max = 16, message = "Maksymalnie 16 znaków")
    private String street;

    @Column
    @NotNull
    @Size(min = 1, message = "Minimalnie 1 znak")
    @Size(max = 16, message = "Maksymalnie 16 znaków")
    private String homeNr;

    @Column
    @Size(max = 16, message = "Maksymalnie 16 znaków")
    private String flatNr;
}
