package pro.backend.mapping;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pro.backend.config.properties.ImageServerProperties;
import pro.backend.dto.ImageDto;
import pro.backend.model.Image;

import javax.annotation.PostConstruct;

@Component
public class ImageMapping {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ImageServerProperties imageServerProperties;

    @PostConstruct
    public void setup() {
        Converter<Integer, String> toImageUrl =
                ctx -> ctx.getSource() == null ? null : imageServerProperties.getBaseUrl() + "/" + ctx.getSource();

        modelMapper.createTypeMap(Image.class, ImageDto.class).addMappings(mapper -> mapper.using(toImageUrl).map(Image::getId, ImageDto::setUrl));
    }
}
