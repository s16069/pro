package pro.backend.dto;

import lombok.Data;
import pro.backend.model.order.OrderedPizza;
import pro.backend.model.user.Address;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class OrderDto {

    @Size(min = 1, message = "Zamów co najmniej jedną pizzę")
    @Size(max = 10, message = "Zamów co najwyżej 10 pizz")
    private List<OrderedPizza> pizzas;

    @Valid
    private Address address;

    private String comments;
}
