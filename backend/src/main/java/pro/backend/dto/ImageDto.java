package pro.backend.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageDto {
    private long id;

    private String url;

    private String alt;

    private String mime;

    private int width;

    private int height;
}
