package pro.backend.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadImageDto {

    private String fileName;

    private String fileData;

    private String alt;
}
