package pro.backend.dto.menu.product;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ProductVariantDto {

    private Long id;

    private String name;

    private String description;

    private BigDecimal priceMultiplier;
}
