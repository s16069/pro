package pro.backend.dto.menu.pizza;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class IngredientDto {

    private Long id;

    private String name;

    private boolean spicy;

    private BigDecimal price;
}
