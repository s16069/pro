package pro.backend.dto.menu.product;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ProductVariantSeriesDto {

    private Long id;

    private String name;

    private String description;

    private List<ProductVariantDto> variants = new ArrayList<>();
}
