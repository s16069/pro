package pro.backend.dto.menu.pizza;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PizzaSizeDto {

    private Long id;

    private String name;

    private int diameter;

    private BigDecimal priceMultiplier;
}
