package pro.backend.dto.menu;

import lombok.Getter;
import lombok.Setter;
import pro.backend.dto.menu.product.MenuProductDto;
import pro.backend.model.menu.PromotionType;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PromotionDto {

    private Long id;

    private String name;

    private PromotionType type;

    private Timestamp dateFrom;

    private Timestamp dateTo;

    private BigDecimal discount;

    private List<MenuProductDto> products = new ArrayList<>();
}
