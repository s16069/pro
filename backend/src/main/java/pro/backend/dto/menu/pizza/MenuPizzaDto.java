package pro.backend.dto.menu.pizza;

import lombok.Getter;
import lombok.Setter;
import pro.backend.dto.menu.MenuPositionDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class MenuPizzaDto extends MenuPositionDto {

    protected Boolean isVegetarian;

    protected Boolean isVegan;

    protected Boolean isSpicy;

    List<IngredientDto> ingredients = new ArrayList<>();

    private BigDecimal basePrice;

    private List<PizzaPriceDto> pizzaPrices = new ArrayList<>();
}
