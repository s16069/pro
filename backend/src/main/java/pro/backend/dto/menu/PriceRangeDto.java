package pro.backend.dto.menu;

import lombok.Getter;
import lombok.Setter;
import lombok.Value;

import java.math.BigDecimal;

@Getter
@Setter
public class PriceRangeDto {
    BigDecimal from;

    BigDecimal to;
}
