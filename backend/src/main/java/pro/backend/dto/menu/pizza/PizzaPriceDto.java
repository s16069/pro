package pro.backend.dto.menu.pizza;


import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PizzaPriceDto {

    protected Long id;

    private PizzaDoughDto dough;

    private PizzaSizeDto size;

    private BigDecimal price;
}