package pro.backend.dto.menu.pizza;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PizzaDoughDto {

    private Long id;

    private String name;

    private String description;

    private BigDecimal priceMultiplier;
}
