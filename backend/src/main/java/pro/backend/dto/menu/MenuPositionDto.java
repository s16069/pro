package pro.backend.dto.menu;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;
import pro.backend.dto.ImageDto;
import pro.backend.dto.menu.pizza.MenuPizzaDto;
import pro.backend.dto.menu.product.MenuProductDto;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = MenuPizzaDto.class, name = "pizza"),
        @JsonSubTypes.Type(value = MenuProductDto.class, name = "product")
})
public abstract class MenuPositionDto {

    @JsonInclude()
    private String type;

    protected Long id;

    protected String name;

    protected String description;

    private ImageDto image;

    List<PromotionDto> promotions = new ArrayList<>();

    List<MenuCategoryDto> categories = new ArrayList<>();

    private PriceRangeDto priceRange;

}
