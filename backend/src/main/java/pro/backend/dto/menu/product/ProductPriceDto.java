package pro.backend.dto.menu.product;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


@Getter
@Setter
public class ProductPriceDto {

    protected Long id;

    private ProductVariantDto variant;

    private BigDecimal price;
}