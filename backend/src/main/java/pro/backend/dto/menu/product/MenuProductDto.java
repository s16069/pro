package pro.backend.dto.menu.product;

import lombok.Getter;
import lombok.Setter;
import pro.backend.dto.menu.MenuPositionDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class MenuProductDto extends MenuPositionDto {

    private BigDecimal basePrice;

    private boolean vegetarian;

    private boolean vegan;

    private List<ProductPriceDto> productPrices = new ArrayList<>();
}
