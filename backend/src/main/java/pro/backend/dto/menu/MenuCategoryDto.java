package pro.backend.dto.menu;

import lombok.Getter;
import lombok.Setter;
import pro.backend.dto.menu.product.MenuProductDto;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class MenuCategoryDto {
    private Long id;

    private String name;

    private String description;

    private List<MenuProductDto> products = new ArrayList<>();
}
