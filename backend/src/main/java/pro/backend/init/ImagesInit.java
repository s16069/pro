package pro.backend.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.service.ImageService;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component("ImagesInit")
public class ImagesInit {

    @Autowired
    private ImageService imageService;

    @PostConstruct
    @Transactional
    private void postConstruct() {
        if (imageService.listAll().size() == 0) {
            addImages();
        }
    }

    private void addImages() {
        try {
            imageService.add("pizza1.png", "pizza1", null, getClass().getClassLoader().getResourceAsStream("images/pizza.png"));
            imageService.add("pizza2.png", "pizza2", null, getClass().getClassLoader().getResourceAsStream("images/pizza.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}