package pro.backend.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.menu.pizza.Ingredient;
import pro.backend.repository.menu.pizza.IngredientRepo;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;

@Component("IngredientInit")
public class IngredientInit {

    @Autowired
    private IngredientRepo ingredientRepo;

    @PostConstruct
    @Transactional
    private void postConstruct() {
        if (ingredientRepo.count() == 0) {
            add();
        }
    }

    private void add() {
        ingredientRepo.save(new Ingredient("Salami", true, new BigDecimal("3.0")));
        ingredientRepo.save(new Ingredient("Ser", false, new BigDecimal("1.0")));
        ingredientRepo.save(new Ingredient("Szynka", false, new BigDecimal("2.0")));

        ingredientRepo.flush();
    }
}