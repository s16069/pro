package pro.backend.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.menu.pizza.PizzaDough;
import pro.backend.repository.menu.pizza.PizzaDoughRepo;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;

@Component("PizzaDoughInit")
public class PizzaDoughInit {

    @Autowired
    private PizzaDoughRepo pizzaDoughRepo;

    @PostConstruct
    @Transactional
    private void postConstruct() {
        if (pizzaDoughRepo.count() == 0) {
            add();
        }
    }

    private void add() {
        pizzaDoughRepo.save(new PizzaDough("Cienkie", "Cienkie", new BigDecimal(1)));
        pizzaDoughRepo.save(new PizzaDough("Grube", "Grube", new BigDecimal(1.2)));

        pizzaDoughRepo.flush();
    }
}
