package pro.backend.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.menu.product.MenuProduct;
import pro.backend.model.menu.product.ProductVariant;
import pro.backend.model.menu.product.ProductVariantSeries;
import pro.backend.repository.menu.product.MenuProductRepo;
import pro.backend.repository.menu.product.ProductPriceRepo;
import pro.backend.repository.menu.product.ProductVariantRepo;
import pro.backend.repository.menu.product.ProductVariantSeriesRepo;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.Arrays;

@Component
@DependsOn({"IngredientInit", "PizzaSizeInit", "PizzaDoughInit"})
public class MenuProductInit {

    @Autowired
    private MenuProductRepo menuProductRepo;

    @Autowired
    private ProductPriceRepo productPriceRepo;

    @Autowired
    private ProductVariantSeriesRepo productVariantSeriesRepo;

    @Autowired
    private ProductVariantRepo productVariantRepo;

    @PostConstruct
    @Transactional
    private void postConstruct() {
        if (menuProductRepo.count() == 0) {
            add();
        }
    }

    private void add() {

        ProductVariantSeries variantSeries = new ProductVariantSeries("Pojemność", "Pojemność");

        ProductVariant v1 = new ProductVariant("0,33", "0,33", new BigDecimal("0.8"), variantSeries);
        ProductVariant v2 = new ProductVariant("0,5", "0,5", new BigDecimal("1.0"), variantSeries);
        ProductVariant v3 = new ProductVariant("0,7", "0,7", new BigDecimal("1.2"), variantSeries);
        variantSeries.getVariants().addAll(Arrays.asList(v1, v2, v3));

        productVariantSeriesRepo.save(variantSeries);
        productVariantSeriesRepo.flush();

        MenuProduct p1 = new MenuProduct("Piwo", "Piwo", new BigDecimal("5.0"), false, false, variantSeries);
        p1.generatePrices();
        menuProductRepo.save(p1);
        menuProductRepo.flush();
    }
}