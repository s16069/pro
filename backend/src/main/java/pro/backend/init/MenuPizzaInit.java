package pro.backend.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.Image;
import pro.backend.model.menu.pizza.Ingredient;
import pro.backend.model.menu.pizza.MenuPizza;
import pro.backend.model.menu.pizza.PizzaDough;
import pro.backend.model.menu.pizza.PizzaSize;
import pro.backend.repository.ImageRepo;
import pro.backend.repository.menu.pizza.IngredientRepo;
import pro.backend.repository.menu.pizza.PizzaDoughRepo;
import pro.backend.repository.menu.pizza.PizzaSizeRepo;
import pro.backend.service.PizzaService;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@Component
@DependsOn({"IngredientInit", "PizzaSizeInit", "PizzaDoughInit", "ImagesInit"})
public class MenuPizzaInit {

    @Autowired
    private PizzaService pizzaService;

    @Autowired
    private IngredientRepo ingredientRepo;

    @Autowired
    private PizzaSizeRepo pizzaSizeRepo;

    @Autowired
    private PizzaDoughRepo pizzaDoughRepo;

    @Autowired
    private ImageRepo imageRepo;


    @PostConstruct
    @Transactional
    private void postConstruct() {
        if (pizzaService.list().size() == 0) {
            addPizzas();
        }
    }

    private void addPizzas() {
        List<Ingredient> ingredients = ingredientRepo.findAll();
        List<PizzaSize> sizes = pizzaSizeRepo.findAll();
        List<PizzaDough> doughs = pizzaDoughRepo.findAll();
        List<Image> images = imageRepo.findAll();

        MenuPizza p1 = new MenuPizza("Carbonara", "Bardzo smaczna", false, false, false, new BigDecimal(20));
        p1.setImage(images.get(0));
        p1.setIngredients(ingredients);
        p1.generatePrices(sizes, doughs);

        pizzaService.add(p1);

        MenuPizza p2 = new MenuPizza("Cappriciosa", "Bardzo smaczna", false, false, false, new BigDecimal(25));
        p2.setImage(images.get(1));
        p2.setIngredients(ingredients);
        p2.generatePrices(sizes, doughs);

        pizzaService.add(p2);
    }
}