package pro.backend.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.menu.pizza.PizzaSize;
import pro.backend.repository.menu.pizza.PizzaSizeRepo;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;

@Component("PizzaSizeInit")
public class PizzaSizeInit {

    @Autowired
    private PizzaSizeRepo pizzaSizeRepo;

    @PostConstruct
    @Transactional
    private void postConstruct() {
        if (pizzaSizeRepo.count() == 0) {
            add();
        }
    }

    private void add() {
        pizzaSizeRepo.save(new PizzaSize("S", 30, new BigDecimal("0.8")));
        pizzaSizeRepo.save(new PizzaSize("M", 40, new BigDecimal("1")));
        pizzaSizeRepo.save(new PizzaSize("L", 50, new BigDecimal("1.2")));
        pizzaSizeRepo.save(new PizzaSize("XL", 60, new BigDecimal("1.4")));

        pizzaSizeRepo.flush();
    }
}
