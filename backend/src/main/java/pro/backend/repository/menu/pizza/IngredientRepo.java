package pro.backend.repository.menu.pizza;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.menu.pizza.Ingredient;

@Repository
public interface IngredientRepo extends JpaRepository<Ingredient, Long> {

}
