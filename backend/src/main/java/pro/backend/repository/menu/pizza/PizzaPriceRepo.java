package pro.backend.repository.menu.pizza;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.menu.pizza.MenuPizza;
import pro.backend.model.menu.pizza.PizzaDough;
import pro.backend.model.menu.pizza.PizzaPrice;
import pro.backend.model.menu.pizza.PizzaSize;

import java.util.Optional;

@Repository
public interface PizzaPriceRepo extends JpaRepository<PizzaPrice, Long> {
    Optional<PizzaPrice> findByMenuPizzaAndDoughAndSize(MenuPizza menuPizza, PizzaDough dough, PizzaSize size);
}
