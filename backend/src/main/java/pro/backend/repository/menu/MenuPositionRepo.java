package pro.backend.repository.menu;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.menu.MenuPosition;

@Repository
public interface MenuPositionRepo extends JpaRepository<MenuPosition, Long> {

}
