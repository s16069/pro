package pro.backend.repository.menu.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.menu.product.ProductVariantSeries;

@Repository
public interface ProductVariantSeriesRepo extends JpaRepository<ProductVariantSeries, Long> {

}
