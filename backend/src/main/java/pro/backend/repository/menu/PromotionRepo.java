package pro.backend.repository.menu;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.menu.Promotion;

@Repository
public interface PromotionRepo extends JpaRepository<Promotion, Long> {

}
