package pro.backend.repository.menu.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.menu.product.ProductVariant;

@Repository
public interface ProductVariantRepo extends JpaRepository<ProductVariant, Long> {

}
