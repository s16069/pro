package pro.backend.repository.menu.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.menu.product.ProductPrice;

@Repository
public interface ProductPriceRepo extends JpaRepository<ProductPrice, Long> {

}
