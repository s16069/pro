package pro.backend.repository.menu;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.menu.MenuCategory;

@Repository
public interface MenuCategoryRepo extends JpaRepository<MenuCategory, Long> {

}
