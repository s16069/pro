package pro.backend.repository.menu.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.menu.product.MenuProduct;

@Repository
public interface MenuProductRepo extends JpaRepository<MenuProduct, Long> {

}
