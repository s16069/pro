package pro.backend.repository.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.order.OrderedProduct;

@Repository
public interface OrderedProductRepo extends JpaRepository<OrderedProduct, Long> {

}
