package pro.backend.repository.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.order.OrderedPizza;

@Repository
public interface OrderedPizzaRepo extends JpaRepository<OrderedPizza, Long> {

}
