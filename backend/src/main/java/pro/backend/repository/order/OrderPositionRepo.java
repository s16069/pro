package pro.backend.repository.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.order.OrderPosition;

@Repository
public interface OrderPositionRepo extends JpaRepository<OrderPosition, Long> {
}
