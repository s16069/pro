package pro.backend.repository.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.order.Order;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface OrderRepo extends JpaRepository<Order, Long> {
    List<Order> findByClientId(String clientId);

    List<Order> findByTimeCreatedBetween(Timestamp from, Timestamp to);
}
