package pro.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pro.backend.model.Image;

@Repository
public interface ImageRepo extends JpaRepository<Image, Long> {

}
