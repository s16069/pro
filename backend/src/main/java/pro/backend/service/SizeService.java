package pro.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.menu.pizza.PizzaSize;
import pro.backend.model.menu.pizza.PizzaSize;
import pro.backend.repository.menu.pizza.PizzaSizeRepo;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = false)
public class SizeService {

    @Autowired
    private PizzaSizeRepo pizzaSizeRepo;

    public List<PizzaSize> list() {
        return pizzaSizeRepo.findAll();
    }

    @Transactional
    public PizzaSize add(PizzaSize size) {
        return pizzaSizeRepo.saveAndFlush(size);
    }

    @Transactional
    public Optional<PizzaSize> update(long id, PizzaSize update) {
        Optional<PizzaSize> existing = pizzaSizeRepo.findById(id);
        if (!existing.isPresent()) {
            return Optional.empty();
        }

        update.setId(id);

        return Optional.of(pizzaSizeRepo.saveAndFlush(update));
    }

    @Transactional
    public void delete(long id) {
        Optional<PizzaSize> existing = pizzaSizeRepo.findById(id);
        existing.ifPresent(size -> pizzaSizeRepo.delete(size));
    }
}
