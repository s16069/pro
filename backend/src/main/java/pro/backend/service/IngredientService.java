package pro.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.menu.pizza.Ingredient;
import pro.backend.repository.menu.pizza.IngredientRepo;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = false)
public class IngredientService {

    @Autowired
    private IngredientRepo ingredientRepo;

    public Optional<Ingredient> findById(long id) {
        return ingredientRepo.findById(id);
    }

    public List<Ingredient> list() {
        return ingredientRepo.findAll();
    }

    @Transactional
    public Ingredient add(Ingredient ingredient) {
        return ingredientRepo.saveAndFlush(ingredient);
    }

    @Transactional
    public Optional<Ingredient> update(long id, Ingredient update) {
        Optional<Ingredient> optional = ingredientRepo.findById(id);
        if (!optional.isPresent()) {
            return Optional.empty();
        }

        Ingredient existing = optional.get();

        existing.setName(update.getName());
        existing.setSpicy(update.isSpicy());
        existing.setPrice(update.getPrice());

        return Optional.of(ingredientRepo.saveAndFlush(existing));
    }

    @Transactional
    public void delete(long id) {
        Optional<Ingredient> optional = ingredientRepo.findById(id);
        if (optional.isPresent()) {
            ingredientRepo.delete(optional.get());
        }
    }
}
