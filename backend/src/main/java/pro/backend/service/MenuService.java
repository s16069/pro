package pro.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.menu.MenuPosition;
import pro.backend.repository.menu.MenuPositionRepo;
import pro.backend.repository.menu.pizza.IngredientRepo;
import pro.backend.repository.menu.pizza.PizzaPriceRepo;

import java.util.List;

@Service
@Transactional(readOnly = false)
public class MenuService {

    @Autowired
    private MenuPositionRepo menuPositionRepo;

    @Autowired
    private PizzaPriceRepo pizzaPriceRepo;

    @Autowired
    private IngredientRepo ingredientRepo;

    public List<MenuPosition> listAll() {
        return menuPositionRepo.findAll();
    }
}
