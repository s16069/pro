package pro.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.menu.pizza.MenuPizza;
import pro.backend.model.menu.pizza.PizzaDough;
import pro.backend.model.menu.pizza.PizzaPrice;
import pro.backend.repository.menu.pizza.PizzaDoughRepo;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = false)
public class DoughService {

    @Autowired
    private PizzaDoughRepo pizzaDoughRepo;

    public List<PizzaDough> list() {
        return pizzaDoughRepo.findAll();
    }

    @Transactional
    public PizzaDough add(PizzaDough dough) {
        return pizzaDoughRepo.saveAndFlush(dough);
    }

    @Transactional
    public Optional<PizzaDough> update(long id, PizzaDough update) {
        Optional<PizzaDough> existing = pizzaDoughRepo.findById(id);
        if (!existing.isPresent()) {
            return Optional.empty();
        }

        update.setId(id);

        return Optional.of(pizzaDoughRepo.saveAndFlush(update));
    }

    @Transactional
    public void delete(long id) {
        Optional<PizzaDough> existing = pizzaDoughRepo.findById(id);
        existing.ifPresent(dough -> pizzaDoughRepo.delete(dough));
    }
}
