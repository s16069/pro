package pro.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.menu.pizza.Ingredient;
import pro.backend.model.menu.pizza.MenuPizza;
import pro.backend.model.menu.pizza.PizzaPrice;
import pro.backend.repository.menu.pizza.IngredientRepo;
import pro.backend.repository.menu.pizza.MenuPizzaRepo;
import pro.backend.repository.menu.pizza.PizzaPriceRepo;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = false)
public class PizzaService {

    @Autowired
    private MenuPizzaRepo menuPizzaRepo;

    @Autowired
    private PizzaPriceRepo pizzaPriceRepo;

    @Autowired
    private IngredientRepo ingredientRepo;

    public List<MenuPizza> list() {
        return menuPizzaRepo.findAll();
    }

    public Optional<MenuPizza> findById(long id) {
        return menuPizzaRepo.findById(id);
    }

    @Transactional
    public MenuPizza add(MenuPizza pizza) {

        List<PizzaPrice> prices = pizza.getPizzaPrices();

        MenuPizza saved = menuPizzaRepo.saveAndFlush(pizza);

        for (PizzaPrice pizzaPrice : prices) {
            pizzaPrice.setId(null);
            pizzaPrice.setMenuPizza(saved);
            pizzaPriceRepo.saveAndFlush(pizzaPrice);
        }

        return saved;
    }

    @Transactional
    public Optional<MenuPizza> update(long id, MenuPizza update) {
        Optional<MenuPizza> optional = menuPizzaRepo.findById(id);
        if (!optional.isPresent()) {
            return Optional.empty();
        }

        MenuPizza existing = optional.get();

        existing.setBasePrice(update.getBasePrice());
        existing.setDescription(update.getDescription());
        existing.setImage(update.getImage());
        existing.setIsSpicy(update.getIsSpicy());
        existing.setIsVegan(update.getIsVegan());
        existing.setIsVegetarian(update.getIsVegetarian());

        return Optional.of(menuPizzaRepo.saveAndFlush(existing));
    }

    @Transactional
    public void delete(long id) {
        Optional<MenuPizza> optional = menuPizzaRepo.findById(id);
        if (optional.isPresent()) {
            menuPizzaRepo.delete(optional.get());
        }
    }

    @Transactional
    public Optional<PizzaPrice> addPrice(long pizzaId, PizzaPrice pizzaPrice) {
        Optional<MenuPizza> optional = menuPizzaRepo.findById(pizzaId);
        if (!optional.isPresent()) {
            return Optional.empty();
        }

        pizzaPrice.setMenuPizza(optional.get());

        return Optional.of(pizzaPriceRepo.save(pizzaPrice));
    }

    @Transactional
    public Optional<PizzaPrice> updatePrice(long pizzaId, long priceId, PizzaPrice update) {
        Optional<PizzaPrice> optional = pizzaPriceRepo.findById(priceId);
        if (!optional.isPresent()) {
            return Optional.empty();
        }

        PizzaPrice existing = optional.get();

        existing.setPrice(update.getPrice());
        existing.setDough(update.getDough());
        existing.setSize(update.getSize());

        return Optional.of(pizzaPriceRepo.saveAndFlush(existing));
    }

    @Transactional
    public void deletePrice(long pizzaId, long priceId) {
        Optional<PizzaPrice> optional = pizzaPriceRepo.findById(priceId);
        if (optional.isPresent()) {
            pizzaPriceRepo.delete(optional.get());
        }
    }

    public Optional<MenuPizza> addIngredient(long pizzaId, long ingredientId) {
        Optional<MenuPizza> pizzaType = menuPizzaRepo.findById(pizzaId);
        if (!pizzaType.isPresent()) {
            return Optional.empty();
        }

        Optional<Ingredient> ingredient = ingredientRepo.findById(ingredientId);
        if (!ingredient.isPresent()) {
            return Optional.empty();
        }

        pizzaType.get().getIngredients().add(ingredient.get());

        return Optional.of(menuPizzaRepo.saveAndFlush(pizzaType.get()));
    }

    public Optional<MenuPizza> deleteIngredient(long pizzaId, long ingredientId) {
        Optional<MenuPizza> pizzaType = menuPizzaRepo.findById(pizzaId);
        if (!pizzaType.isPresent()) {
            return Optional.empty();
        }

        Optional<Ingredient> ingredient = ingredientRepo.findById(ingredientId);
        if (!ingredient.isPresent()) {
            return Optional.empty();
        }

        pizzaType.get().getIngredients().remove(ingredient.get());

        return Optional.of(menuPizzaRepo.saveAndFlush(pizzaType.get()));
    }
}
