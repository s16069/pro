package pro.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.Image;
import pro.backend.model.menu.MenuPosition;
import pro.backend.model.menu.pizza.MenuPizza;
import pro.backend.repository.ImageRepo;
import pro.backend.repository.menu.MenuPositionRepo;
import pro.backend.repository.menu.pizza.IngredientRepo;
import pro.backend.repository.menu.pizza.PizzaPriceRepo;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = false)
public class ImageService {

    @Autowired
    private ImageRepo imageRepo;

    public List<Image> listAll() {
        return imageRepo.findAll();
    }

    public Optional<Image> findById(long id) {
        return imageRepo.findById(id);
    }

    public Image add(String name, String alt, String userId, InputStream inputStream) throws IOException {
        Image image = new Image();
        image.setName(name);
        image.setAlt(alt);

        byte[] data = inputStream.readAllBytes();

        BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(data));
        image.setWidth(bufferedImage.getWidth());
        image.setHeight(bufferedImage.getHeight());
        image.setMime(URLConnection.guessContentTypeFromName(name));

        //image.setUserId(userId);
        image.setTimeCreated(Timestamp.from(Instant.now()));

        image.setData(data);
        image.setSize(image.getData().length);

        return imageRepo.saveAndFlush(image);
    }

//    private String getMime(String name, String ext, byte[] data) throws IOException {
//        Path tempFile = null;
//        try {
//            tempFile = Files.createTempFile(name, "." + ext);
//            Files.write(tempFile, data);
//            return Files.probeContentType(tempFile);
//        } finally {
//            if (tempFile != null) {
//                Files.delete(tempFile);
//            }
//        }
//    }

}
