package pro.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.menu.Promotion;
import pro.backend.repository.menu.PromotionRepo;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = false)
public class PromotionService {

    @Autowired
    private PromotionRepo promotionRepo;

    public Optional<Promotion> findById(long id) {
        return promotionRepo.findById(id);
    }

    public List<Promotion> list() {
        return promotionRepo.findAll();
    }

    @Transactional
    public Promotion add(Promotion promotion) {

        promotion.setId(null);

        return promotionRepo.saveAndFlush(promotion);
    }

    @Transactional
    public Optional<Promotion> update(long id, Promotion update) {
        Optional<Promotion> optional = promotionRepo.findById(id);
        if (!optional.isPresent()) {
            return Optional.empty();
        }

        Promotion existing = optional.get();

        existing.setName(update.getName());
        existing.setType(update.getType());
        existing.setDiscount(update.getDiscount());
        existing.setDateFrom(update.getDateFrom());
        existing.setDateTo(update.getDateTo());

        return Optional.of(promotionRepo.saveAndFlush(existing));
    }

    @Transactional
    public void delete(long id) {
        Optional<Promotion> optional = promotionRepo.findById(id);
        if (optional.isPresent()) {
            promotionRepo.delete(optional.get());
        }
    }
}
