package pro.backend.service;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.menu.pizza.MenuPizza;
import pro.backend.model.menu.pizza.PizzaDough;
import pro.backend.model.menu.pizza.PizzaPrice;
import pro.backend.model.menu.pizza.PizzaSize;
import pro.backend.model.order.Order;
import pro.backend.model.order.OrderStatus;
import pro.backend.model.order.Order_;
import pro.backend.model.order.OrderedPizza;
import pro.backend.model.user.Address;
import pro.backend.model.user.Roles;
import pro.backend.repository.menu.pizza.MenuPizzaRepo;
import pro.backend.repository.menu.pizza.PizzaPriceRepo;
import pro.backend.repository.order.OrderRepo;
import pro.backend.repository.order.OrderedPizzaRepo;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = false)
public class OrderService {

    @Autowired
    private MenuPizzaRepo menuPizzaRepo;

    @Autowired
    private PizzaPriceRepo pizzaPriceRepo;

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private OrderedPizzaRepo orderedPizzaRepo;

    @Transactional
    public Optional<Order> addOrder(String userId, List<OrderedPizza> orderedPizzas, Address address, String comments) {
        Order order = new Order();

        List<OrderedPizza> positions = new ArrayList<>();
        for (OrderedPizza inOrderedPizza : orderedPizzas) {
            Long orderedPizzaId = inOrderedPizza.getMenuPizza().getId();
            if (orderedPizzaId == null) {
                return Optional.empty();
            }

            Optional<MenuPizza> optionalPizzaType = menuPizzaRepo.findById(orderedPizzaId);
            if (!optionalPizzaType.isPresent()) {
                return Optional.empty();
            }

            MenuPizza menuPizza = optionalPizzaType.get();

            PizzaSize size = inOrderedPizza.getSize();
            PizzaDough dough = inOrderedPizza.getDough();

            Optional<PizzaPrice> price = pizzaPriceRepo.findByMenuPizzaAndDoughAndSize(menuPizza, dough, size);
            if (!price.isPresent()) {
                return Optional.empty();
            }

            int amount = inOrderedPizza.getAmount();

            OrderedPizza orderedPizza = new OrderedPizza();
            orderedPizza.setOrder(order);
            orderedPizza.setAmount(amount);
            orderedPizza.setMenuPizza(menuPizza);
            orderedPizza.setDough(dough);
            orderedPizza.setSize(size);
            orderedPizza.setPrice(price.get());

            positions.add(orderedPizza);
        }

        setStatus(order, OrderStatus.CREATED);
        order.setClientId(userId);
        order.setAddress(address);
        order.setComments(comments);

        orderRepo.saveAndFlush(order);

        order.setPositions(positions);

        orderedPizzaRepo.saveAll(positions);


        orderRepo.saveAndFlush(order);

        return orderRepo.findById(order.getId());
    }

    public static List<Order> listNewOrders(Session session) {
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Order> criteria = criteriaBuilder.createQuery(Order.class);

        Root<Order> orderRoot = criteria.from(Order.class);
        criteria.select(orderRoot);
        criteria.where(criteriaBuilder.equal(orderRoot.get(Order_.status), OrderStatus.CREATED));

        return session.createQuery(criteria).getResultList();
    }

    public List<Order> list(String userId, List<String> roles) {

        boolean client = isClient(roles);

        if (client) {
            return orderRepo.findByClientId(userId);
        } else {
            Timestamp from = Timestamp.from(Instant.now().minus(1, ChronoUnit.DAYS));
            Timestamp to = Timestamp.from(Instant.now());
            return orderRepo.findByTimeCreatedBetween(from, to);
        }
    }

    public Optional<Order> get(String userId, List<String> roles, long orderId) {
        Optional<Order> order = orderRepo.findById(orderId);

        if (!order.isPresent() || !canView(userId, roles, order.get())) {
            return Optional.empty();
        }

        return order;
    }

    private boolean canView(String userId, List<String> roles, Order order) {
        boolean client = isClient(roles);

        return !client || order.getClientId().equals(userId);
    }

    private boolean isClient(List<String> roles) {
        return roles.stream().anyMatch(role -> role.equals(Roles.ROLE_CLIENT));
    }

    @Transactional
    public Optional<Order> changeStatus(long orderId, OrderStatus newStatus) {
        Optional<Order> optional = orderRepo.findById(orderId);

        if (!optional.isPresent()) {
            return Optional.empty();
        }

        Order order = optional.get();

        OrderStatus currentStatus = order.getStatus();
        int currentOrdinal = currentStatus.ordinal();
        int newOrdinal = newStatus.ordinal();

        if (newOrdinal != currentOrdinal + 1) {
            throw new IllegalArgumentException("Nie można zmienić stanu z " + currentStatus + " na " + newStatus);
        }

        setStatus(order, newStatus);

        return Optional.of(orderRepo.saveAndFlush(order));
    }

    private void setStatus(Order order, OrderStatus newStatus) {
        order.setStatus(newStatus);
        Timestamp timestamp = Timestamp.from(Instant.now());
        switch (newStatus) {
            case CREATED:
                order.setTimeCreated(timestamp);
                break;
            case ACCEPTED:
                order.setTimeAccepted(timestamp);
                break;
            case PREPARED:
                order.setTimePrepared(timestamp);
                break;
            case DELIVERED:
                order.setTimeDelivered(timestamp);
                break;
        }
    }
}
