package pro.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.backend.model.menu.product.MenuProduct;
import pro.backend.repository.menu.product.MenuProductRepo;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = false)
public class ProductService {

    @Autowired
    private MenuProductRepo menuProductRepo;

    public List<MenuProduct> listProducts() {
        return menuProductRepo.findAll();
    }

    public Optional<MenuProduct> findById(long id) {
        return menuProductRepo.findById(id);
    }
}
