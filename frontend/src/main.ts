import Vue from 'vue'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import './scss/common.scss'

import App from './App.vue'
import router from './router'
import store from './store'
import configureValidation from './validation';
import autoRegisterBaseComponents from './components/base/autoRegister';

Vue.config.productionTip = process.env.NODE_ENV === 'development';
Vue.config.devtools = process.env.NODE_ENV === 'development';

Vue.use(BootstrapVue);

configureValidation();
autoRegisterBaseComponents();

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
