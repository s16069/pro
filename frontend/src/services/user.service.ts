import Keycloak from 'keycloak-js';

import store from '@/store'

let initOptions: Keycloak.KeycloakConfig = {
  url: 'http://127.0.0.1:9020/auth',
  realm: 'Pizzeria',
  clientId: 'pizzeria',
}

const keycloak = Keycloak(initOptions);

keycloak.init({}).then((authenticated) => {
	if (!authenticated) {
		console.info("Not authenticated");
	} else {
		console.log(keycloak.tokenParsed);

		const token = keycloak.tokenParsed || {};

		const username = token['preferred_username'];
		const roles = token.realm_access!.roles || [];

		store.commit('user/setToken', keycloak.token);
		store.commit('user/setUsername', username);
		store.commit('user/setRoles', roles);

		console.info("Authenticated");
		setupRefresh();
	}

}).catch((error) => {
  console.error("Auth initialization failed", error);
});

function setupRefresh() {
	setInterval(() => {
		keycloak.updateToken(70).then((refreshed) => {
			if (refreshed) {
				console.info('Token refreshed');
				
			} else {
				const validFor = Math.round(keycloak.tokenParsed!.exp! + keycloak.timeSkew! - new Date().getTime() / 1000);
				console.info(`Token not refreshed, valid for ${validFor} seconds`);
			}
		}).catch((error) => {
			console.error('Failed to refresh token', error);
		});
	}, 6000)
}

export function login() {
	keycloak.login().then(() => {
		console.info('Redirected to login page')
	});
}

export function logout() {
	store.commit('user/clear');
	return keycloak.logout().then(() => {
		console.info('Logged out')
	});
}

export function register() {
	keycloak.login({action: 'register'}).then(() => {
		console.info('Redirected to register page')
	});
}

export function profile() {
	keycloak.accountManagement().then(() => {
		console.info('Redirected to profile page')
	});
}