export default class ForbiddenError extends Error {
	constructor() {
		super('Brak uprawnień do wykonania akcji');
		Object.setPrototypeOf(this, ForbiddenError.prototype);
	}
}
