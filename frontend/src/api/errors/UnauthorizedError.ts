export default class UnauthorizedError extends Error {
	constructor() {
		super('Sesja wygasła');
		Object.setPrototypeOf(this, UnauthorizedError.prototype);
	}
}
