export default class MessageError extends Error {
	constructor(message: string) {
		super(message);
		Object.setPrototypeOf(this, MessageError.prototype);
	}
}
