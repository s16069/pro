export default class CommunicationError extends Error {
	constructor() {
		super('Błąd komunikacji');
		Object.setPrototypeOf(this, CommunicationError.prototype);
	}
}
