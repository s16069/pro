
import axios from 'axios'

import store from '@/store'

import { Configuration } from './generated/configuration';
import {
	AdminDoughControllerApi,
	AdminImagesControllerApi,
	AdminIngredientControllerApi,
	AdminOrderControllerApi,
	AdminPizzaControllerApi,
	AdminPizzaIngredientsControllerApi,
	AdminPizzaPricesControllerApi,
	AdminPromotionControllerApi,
	AdminSizeControllerApi,
	DoughControllerApi,
	ImagesControllerApi,
	IngredientControllerApi,
	OrderControllerApi,
	PizzaControllerApi,
	ProductControllerApi,
	PromotionControllerApi,
	SizeControllerApi,
} from './generated/api';

import {
	UnauthorizedError,
	ForbiddenError,
	MessageError,
	CommunicationError,
} from './errors'


const AUTHORIZATION_HEADER = "authorization"

const configuration: Configuration = {};

const basePath = process.env.NODE_ENV === 'development' ? 'http://127.0.0.1:9000' : 'https://test.agrave.pl/api';

const axiosInstance = axios.create();

axiosInstance.interceptors.request.use(function (config) {
	if (store.state.user.token) {
		config.headers[AUTHORIZATION_HEADER] = 'Bearer ' + store.state.user.token;
	}

	return config;
});

axiosInstance.interceptors.response.use(function (response) {
	return response;
}, function (error) {
	console.error('Request error', error);

	if (error.response && error.response.status === 401) {
		return Promise.reject(new UnauthorizedError());
	}

	if (error.response && error.response.status === 403) {
		return Promise.reject(new ForbiddenError());
	}

	if (error.response && error.response.data && error.response.data.message) {
		return Promise.reject(new MessageError(error.response.data.message));
	}

	return Promise.reject(new CommunicationError());
});

export const adminDoughControllerApi = new AdminDoughControllerApi(configuration, basePath, axiosInstance);
export const adminImagesControllerApi = new AdminImagesControllerApi(configuration, basePath, axiosInstance);
export const adminIngredientControllerApi = new AdminIngredientControllerApi(configuration, basePath, axiosInstance);
export const adminOrderControllerApi = new AdminOrderControllerApi(configuration, basePath, axiosInstance);
export const adminPizzaControllerApi = new AdminPizzaControllerApi(configuration, basePath, axiosInstance);
export const adminPizzaIngredientsControllerApi = new AdminPizzaIngredientsControllerApi(configuration, basePath, axiosInstance);
export const adminPizzaPricesControllerApi = new AdminPizzaPricesControllerApi(configuration, basePath, axiosInstance);
export const adminPromotionControllerApi = new AdminPromotionControllerApi(configuration, basePath, axiosInstance);
export const adminSizeControllerApi = new AdminSizeControllerApi(configuration, basePath, axiosInstance);
export const doughControllerApi = new DoughControllerApi(configuration, basePath, axiosInstance);
export const imagesControllerApi = new ImagesControllerApi(configuration, basePath, axiosInstance);
export const ingredientControllerApi = new IngredientControllerApi(configuration, basePath, axiosInstance);
export const orderControllerApi = new OrderControllerApi(configuration, basePath, axiosInstance);
export const pizzaControllerApi = new PizzaControllerApi(configuration, basePath, axiosInstance);
export const productControllerApi = new ProductControllerApi(configuration, basePath, axiosInstance);
export const promotionControllerApi = new PromotionControllerApi(configuration, basePath, axiosInstance);
export const sizeControllerApi = new SizeControllerApi(configuration, basePath, axiosInstance);
