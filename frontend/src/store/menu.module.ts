import { MenuPizzaDto, PizzaDoughDto, PizzaSizeDto } from './../api/generated/api';
import { Module } from 'vuex';
import { pizzaControllerApi, sizeControllerApi, doughControllerApi } from '@/api';
import { RootState } from '.';

export interface MenuState {
	pizzas: MenuPizzaDto[];
	sizes: PizzaDoughDto[];
	doughs: PizzaSizeDto[];
}

const namespaced: boolean = true;

export const MenuModule: Module<MenuState, RootState> = {
	namespaced,
	state: {
		pizzas: [],
		sizes: [],
		doughs: [],
	},
	getters: {

	},
	mutations: {
		setPizzas(state, pizzas: MenuPizzaDto[]) {
			state.pizzas = pizzas;
		},

		setSizes(state, sizes: PizzaSizeDto[]) {
			state.sizes = sizes;
		},

		setDoughs(state, doughs: PizzaDoughDto[]) {
			state.doughs = doughs;
		},
	},
	actions: {
		async listPizzas(context) {
			const response = await pizzaControllerApi.listPizzas1();
			const pizzas = response.data;

			context.commit("setPizzas", pizzas);
		},

		async listSizes(context) {
			const response = await sizeControllerApi.listSizes1();
			const sizes = response.data;

			context.commit("setSizes", sizes);
		},

		async listDoughs(context) {
			const response = await doughControllerApi.listDoughs1();
			const doughs = response.data;

			context.commit("setDoughs", doughs);
		}
	},
};