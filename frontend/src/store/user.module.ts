import { Module } from 'vuex';
import { RootState } from '.';
//import Role from '@/interfaces/Role';

export interface UserState {
	token?: string;
	username?: string;
	roles: string[],
}

const namespaced: boolean = true;

export const UserModule: Module<UserState, RootState> = {
	namespaced,
	state: {
		token: undefined,
		roles: [],
	},
	getters: {
		isLoggedIn(state): boolean {
			return !!state.token;
		},
		isRestaurantAdmin(state, getters): boolean {
			return state.roles.includes('ROLE_RESTAURANT_ADMIN');
		},
		isRestaurantEmployee(state, getters): boolean {
			return state.roles.includes('ROLE_RESTAURANT_EMPLOYEE');
		},
	},
	mutations: {
		setToken(state, token: string) {
			state.token = token;
		},

		setUsername(state, username: string) {
			state.username = username;
		},

		setRoles(state, roles: string[]) {
			state.roles = roles;
		},

		clear(state) {
			state.token = undefined;
			state.username = undefined;
			state.roles = [];
		},
	},
	actions: {
		logout(context) {
			context.commit("clear");
		},
	},
};