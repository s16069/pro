import Vue from "vue";
import Vuex, { StoreOptions } from "vuex";
import VuexPersistence from 'vuex-persist'

import { UserModule, UserState } from "./user.module";
import { MenuModule, MenuState } from "./menu.module";
import { CartModule, CartState } from "./cart.module";
import { AdminState, AdminModule } from './admin.module';

Vue.use(Vuex);

export interface RootState {
	user: UserState;
	menu: MenuState;
	order: CartState;
	admin: AdminState;
}

const vuexLocal = new VuexPersistence<RootState>({
	storage: window.localStorage,
	modules: ['user', 'cart'],
})

const store: StoreOptions<RootState> = {
	modules: {
		user: UserModule,
		menu: MenuModule,
		cart: CartModule,
		admin: AdminModule,
	},
	plugins: [vuexLocal.plugin],
};

export default new Vuex.Store<RootState>(store);
