import { OrderedPizza } from './../api/generated/api';
import { Module } from 'vuex';
import { RootState } from '.';

export interface CartState {
	cartPositions: OrderedPizza[]
}

const namespaced: boolean = true;

export const CartModule: Module<CartState, RootState> = {
	namespaced,
	state: {
		cartPositions: []
	},
	getters: {
	},
	mutations: {
		addPosition(state, pizza: any) {
			state.cartPositions.push(pizza);
		},

		editPosition(state, pizza: any) {
			const idx = state.cartPositions.indexOf(pizza);
			state.cartPositions.splice(idx, 1, pizza);
		},

		removePosition(state, pizza: any) {
			const idx = state.cartPositions.indexOf(pizza);
			state.cartPositions.splice(idx, 1);
		},

		resetCart(state) {
			state.cartPositions = [];
		}
	},
	actions: {
	},
};