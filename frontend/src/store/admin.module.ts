import { imagesControllerApi } from './../api/index';
import { ImageDto } from './../api/generated/api';
import { Module } from 'vuex';
import { RootState } from '.';

export interface AdminState {
	images: ImageDto[];
}

const namespaced: boolean = true;

export const AdminModule: Module<AdminState, RootState> = {
	namespaced,
	state: {
		images: [],
	},
	getters: {

	},
	mutations: {
		setImages(state, images: ImageDto[]) {
			state.images = images;
		},
	},
	actions: {
		async listImages(context) {
			const response = await imagesControllerApi.listImages();
			const images = response.data;

			context.commit("setImages", images);
		},
	},
};