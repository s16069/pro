import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import store from './store'


import PageMain from './pages/PageMain.vue'
import PageCart from './pages/PageCart.vue'
import PageRegister from './pages/PageRegister.vue'
import PageAccount from './pages/PageAccount.vue'
import PageEditMenu from './pages/admin/PageEditMenu.vue'
import PageEditPizza from './pages/admin/PageEditPizza.vue'
import PageAddPizza from './pages/admin/PageAddPizza.vue'
import PageOrder from './pages/PageOrder.vue'
import PageOrders from './pages/PageOrders.vue'

Vue.use(VueRouter)

const routes: RouteConfig[] = [
	{ path: '*', redirect: '/' },
	{ name: 'main', path: '/', component: PageMain, meta: { title: 'Menu' } },
	{ name: 'cart', path: '/cart', component: PageCart, meta: { title: 'Koszyk' } },
	{ name: 'admin-menu', path: '/admin/menu', component: PageEditMenu, meta: { title: 'Edycja menu' } },
	{ name: 'admin-pizza-add', path: '/admin/pizza', component: PageEditPizza, meta: { title: 'Edycja pizzy' } },
	{ name: 'admin-pizza-edit', path: '/admin/pizza/:pizzaId', component: PageEditPizza, meta: { title: 'Edycja pizzy' } },
	{ name: 'add-pizza', path: '/cart/pizza/:pizzaId', component: PageAddPizza, meta: { title: 'Dodaj pizzę' } },
	{ name: 'orders', path: '/orders', component: PageOrders, meta: { title: 'Zamówienia' } },
	{ name: 'order', path: '/order/:orderId', component: PageOrder, meta: { title: 'Zamówienie' } },
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
})

router.beforeEach((to, from, next) => {
	// redirect to login page if not logged in and trying to access a restricted page
	const publicPages = ['/', '/login', '/register'];
	const authRequired = !publicPages.includes(to.path);
	const loggedIn = !!store.state.user.token

	if (authRequired && !loggedIn) {
		return next('/login');
	}

	document.title = to.meta.title;

	next();
})

export default router
