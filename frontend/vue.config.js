module.exports = {
	lintOnSave: false,
	devServer: {
		host: '127.0.0.1',
		disableHostCheck: true,
	},
	chainWebpack: config => {
		config
			.plugin('fork-ts-checker')
			.tap(args => {
				args[0].memoryLimit = 1024;
				return args
			})
	}
}
